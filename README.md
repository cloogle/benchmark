# Cloogle benchmark

This is a stripped-down version of the [Cloogle][] search engine. It is meant
to be able to run benchmarks on implementations of the [Clean][] runtime system
that may not have file I/O. Hence, the database and the queries to execute are
preloaded into the executable.

### Prerequisites

- The database: `types.json` from
  https://files.camilstaps.nl/cloogle-dumps/latest/.
- A CSV file with queries (that yield at least one result). For privacy
  reasons, this log can only be obtained by contacting the Cloogle maintainer.

### Usage

To build:

```bash
git clone --recursive https://gitlab.com/cloogle/benchmark
cd benchmark
```

Put the prerequisites (see above) in this directory as well. Then run
`./compile.sh`, which performs the following steps:

1. Create `clooglelog.icl` from `log_queries.csv`.
1. Compile `clooglebenchmark.icl`, and patch it to preload the database from
   `types.json`.

[Clean]: http://clean.cs.ru.nl
[Cloogle]: https://cloogle.org
