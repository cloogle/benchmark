#!/bin/bash

patch_abc () {
	IFS=''
	while read -r line; do
		if [[ "$line" == *"json_placeholder"* ]]; then
			cat "$1"
		else
			echo -E "$line"
		fi
	done
}

set -ev

# Create clooglelog.icl
cp build.prj.default build.prj
cpm build.prj
./build log > clooglelog.icl

# Read types.json and export it with generic JSON to tmp-db
./build db > tmp-db

# Build clooglebenchmark for the first time
rm -f Clean\ System\ Files/cloogledb.abc
cp clooglebenchmark.prj.default clooglebenchmark.prj
cpm clooglebenchmark.prj
# Replace json_placeholder of cloogledb with JSON generated above
patch_abc tmp-db < Clean\ System\ Files/cloogledb.abc | sponge Clean\ System\ Files/cloogledb.abc
# Force recompile
sleep 1; rm Clean\ System\ Files/cloogledb.o
cpm clooglebenchmark.prj
