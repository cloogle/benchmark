implementation module cloogledb

import StdEnv

import Data.Bifunctor
import Data.Functor
import Data.Func
import Data.Maybe
import Data.Tuple
import qualified Database.Native
import Text.GenJSON

import Cloogle.DB

derive JSONDecode (,,,,,,,,,,,,,,), NGramIndex, Map, 'Database.Native'.Index

JSONDecode{|{#Index}|} infield json = first (fmap \xs -> {#x \\ x <- xs}) $ JSONDecode{|*|} infield json
JSONDecode{|[!!]|} fx infield json = first (fmap \xs -> [|x \\ x <- xs]) $ JSONDecode{|*->*|} fx infield json

db :: *CloogleDB
db = db_from_json json
where
	db_from_json json
	# (Just (es,name_ngrams,name_map,types,core,apps,builtins,syntax,abc_instrs,type_synonyms,
			library_map,module_map,derive_map,instance_map,always_unique)) = fromJSON json
	# db = 'Database.Native'.newDB es
	= {db=db,name_ngrams=name_ngrams,name_map=name_map,
			types=types,core=core,apps=apps,builtins=builtins,syntax=syntax,abc_instrs=abc_instrs,type_synonyms=type_synonyms,
			library_map=library_map,module_map=module_map,derive_map=derive_map,instance_map=instance_map,always_unique=always_unique}

json :: JSONNode
json = fromString "json_placeholder"
