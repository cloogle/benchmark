module build

import StdEnv
import StdMaybe
import StdOverloadedList

import Data.Error
import Data.Func
import qualified Database.Native
import System.CommandLine
import System.File
import Text.GenJSON
import Text.GenPrint

import Cloogle.DB

derive gPrint JSONNode

derive JSONEncode (,,,,,,,,,,,,,,), NGramIndex, Map, 'Database.Native'.Index

JSONEncode{|{#Index}|} infield xs = JSONEncode{|*|} infield [x \\ x <-: xs]
JSONEncode{|[!!]|} fx infield xs = JSONEncode{|*->*|} fx infield [x \\ x <|- xs]

Start w
# ([prog:args],w) = getCommandLine w
# usage = "Usage: "+++prog+++" db|log\n"
| length args <> 1 = abort usage
| otherwise = case hd args of
	"db"
		/** Create a JSON representation of the database */
		# (ok,f,w) = fopen "types.json" FReadText w
		| not ok -> abort "Failed to open types.json; get this from https://files.camilstaps.nl/cloogle-dumps/latest/\n"
		# (db,f) = openDB f
		| isNothing db -> abort "types.json could not be parsed\n"
		# (_,w) = fclose f w
		# json = db_to_json (fromJust db)
		# json = [c \\ c <-: toString json]
		# (io,w) = stdio w
		# io = io <<< "\tbuildAC \""
		# io = write_escaped_chars json io
		# io = io <<< "\"\n"
		# (_,w) = fclose io w
		-> w
	"log"
		/** Copy the logs to a Clean module */
		# (file,w) = readFileLines "log_queries.csv" w
		| isError file -> abort "log_queries.csv could not be read; get this file from the Cloogle maintainer\n"
		# [hd:lines] = fromOk file
		# lines = take 10000 $ filter ((>) 40 o size) lines // too many entries leads to a stack overflow in the compiler
		# (io,w) = stdio w
		# io = io <<< "implementation module clooglelog\nqueries :: [String]\nqueries =\n\t[\""
		# io = foldr
			(\line io -> write_escaped_chars [c \\ c <-: line] (io <<< ",\"") <<< "\"\n\t")
			(write_escaped_chars [c \\ c <-: hd] io <<< "\"\n\t") lines
		# io = io <<< "]"
		# (_,w) = fclose io w
		-> w
	_
		-> abort usage

db_to_json :: !CloogleDB -> JSONNode
db_to_json {db,name_ngrams,name_map,types,core,apps,builtins,syntax,abc_instrs,type_synonyms,library_map,module_map,derive_map,instance_map,always_unique}
# (es,db) = 'Database.Native'.allEntries db
= toJSON
	( Reverse es
	, name_ngrams, name_map
	, types
	, core, apps, builtins, syntax, abc_instrs, type_synonyms
	, library_map, module_map, derive_map, instance_map, always_unique
	)

write_escaped_chars :: ![Char] !*File -> *File
write_escaped_chars [] io = io
write_escaped_chars [c:cs] io = write_escaped_chars cs io`
where
	io`
		| c < ' ' || toInt c>127
			= let ci = toInt c in io <<< "\\" <<< oct (ci >> 6) <<< oct ((ci >> 3) bitand 07) <<< oct (ci bitand 07)
		| c=='\\' || c=='"'
			= io <<< '\\' <<< c
			= io <<< c

	oct i = "01234567".[i]
