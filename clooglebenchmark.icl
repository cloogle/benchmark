implementation module clooglebenchmark

import StdEnv
import StdOverloadedList

import Clean.Types.Parse
import Data.Error
import Data.Func
import Data.Functor
import Data.Maybe
import Data.Tuple

import Cloogle.API
import Cloogle.DB
import Cloogle.Search

import cloogledb
import clooglelog

Start = benchmark

benchmark :: Int
benchmark
# db = foldr do_query db queries
# (entries,db) = getEntries db
= Length entries

do_query :: !String !*CloogleDB -> *CloogleDB
do_query qstring db
# q = parseSingleLineRequest qstring
| isError q = abort ("could not parse '"+++qstring+++"'\n")
#! (resp,db) = handle (fromOk q) db
| length resp.data < 0
	= abort "less than 0 results; should not happen\n"
	= resetDB db

handle :: !Request !*CloogleDB -> *(!Response, !*CloogleDB)
handle request=:{unify,name} db
	| isJust name && size (fromJust name) > 40
		= (err InvalidName "Function name too long" Nothing, db)
	| isJust name && any isSpace (fromString $ fromJust name)
		= (err InvalidName "Name cannot contain spaces" Nothing, db)
	| isJust unify && isNothing (parseType $ fromString $ fromJust unify)
		= (err InvalidType "Couldn't parse type" Nothing, db)
	| all isNothing [unify,name,request.exactName,request.typeName,request.className] && isNothing request.using
		= (err InvalidInput "Empty query" Nothing, db)
	// Results
	#! (results,suggs,db) = searchWithSuggestions request db
	#! suggs = if (isEmpty suggs) Nothing (Just suggs)
	#! results = [r \\ r <|- results]
	// Suggestions
	#! suggs = sortBy ((<) `on` snd) <$> map (appSnd length) <$> suggs
	// Response
	#! response = if (isEmpty results)
		(err NoResults "No results" Nothing)
		{ zero
		& data           = results
		, more_available = Nothing
		, suggestions    = Nothing
		}
	= (response, db)

err :: !CloogleError !String !(Maybe [(Request,Int)]) -> Response
err c m suggs =
	{ return         = toInt c
	, data           = []
	, msg            = m
	, more_available = Nothing
	, suggestions    = suggs
	}
